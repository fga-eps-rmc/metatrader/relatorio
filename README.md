# Relatorio NeuralTrade

## Semana 1 (01/04 - 05/04)

### Atividades Desenvolvidas
<!-- Todos da equipe respondem -->

| Matricula | Nome                              | Gitlab    | Atividade Desenvolvida | Desafios                                                      | Soluções                  |
| --------- | --------------------------------- | --------- | ---------------------- | ------------------------------------------------------------- | ------------------------- |
| 202045624 | Abdul Hannan    | hannanhunny01 | Canvas MVP , Visao Inicial           | Determinar características essenciais do projeto. | Realizar o Lean Inception , realizar pesquisa|
| 180136925 | Hugo Rocha    | hugorochaffs | Canvas MVP             | Refletir e filtrar ideias sobre o projeto | Realizar o Lean Inception |
| 202016462 | Heitor Marques | heitormsb | Canvas MVP | Idealizar de maneira condizente as características do projeto | Realizar o Lean Inception |
| 202016462 | Heitor Marques | heitormsb | Entendimento do Escopo | Pesquisar em vídeos e artigos sobre MT5 e EAs | Realizar a pesquisa |
| 202029012 | Josué Teixeira | zjosuez  | Participei do canvas MVP| Criação do Lean Inception | Organizar as atividade do Lean Inception |
| 202029012 | Josué Teixeira | zjosuez  | Iniciei os estudos sobre Deep Learning| Realizei pesquisas na área e comecei a aprender sobre rede neural | Estudos sobre machine learning |



## Semana 2 (05/04 - 16/04)

### Atividades Desenvolvidas
<!-- Todos da equipe respondem -->

| Matricula | Nome                              | Gitlab    | Atividade Desenvolvida | Desafios                                                      | Soluções                  |    Referecnia Issue |
| --------- | --------------------------------- | --------- | ---------------------- | ------------------------------------------------------------- | ------------------------- | ----|
| 202045624 | Abdul Hannan    | hannanhunny01 |  Backlog ,Definicao de Cronograma,              | Escolher algoritmos para redes neurias | Criar Backlog, <br>  Planejamneto de Sprints,<br>Pesquisa e Definição de estrategias para implementar diferente algoritmos de redes neural| [Issue 01](https://gitlab.com/fga-eps-rmc/neuraltrade/neuraltrade_doc/-/issues/1)|
| 180136925 | Hugo Rocha    | hugorochaffs | Backlog do produto             | pesquisa de tecnologias, discussão sobre sprints e issues e implementação do algoritmo de rss    | Realizar o Backlog <br> Plano de Atividades(Cronograma), |[Issue 04](https://gitlab.com/fga-eps-rmc/neuraltrade/neuraltrade_doc/-/issues/4)|
| 202016462 | Heitor Marques | heitormsb |  Definir Plano de Atividades(Cronograma), Coleta e Preparação de Dados Históricos      | Entender as regras de negocio |Criar backlog,<br> Desenvolver plano de atividades <br>  Desenvolver e estudar a documentação <br>implementação de código para extrair dados(forex)|[Issue 02](https://gitlab.com/fga-eps-rmc/neuraltrade/neuraltrade_doc/-/issues/2)|
| 202029012 | Josué Teixeira | zjosuez | Definir Plano de Atividades(Cronograma), Criação dos algoritmos básicos | Estudo e implementação dos algoritmos  | Desenvonver algoritmos como: média movel, bandas de bollinger e MACD| [Issue 03](https://gitlab.com/fga-eps-rmc/neuraltrade/neuraltrade_doc/-/issues/3) |

### Intercorrências
<!-- Área reservada ao líder técnico -->

| Intercorrência                                           | Intervenção                                                                               |
| -------------------------------------------------------- | ----------------------------------------------------------------------------------------- |
|identificar os primeiros passos para implementar os algoritmos convencionais e redes neurais mais básicas e fáceis para desenvolver | Criação de cronograma e backlog para ter um caminho de desenvolvimento claro e Pesquisa em algoritmos de redes neurais e convencionais para implementar algoritmos mais fáceis no início para obter mais empreendimentos para o futuro|


## Histórico de versão
| Data | Versão | Descrição | Autor(es) | 
| ---- | ---- | ---- | ---- |
| 04/04/2024 | 1.0 | Criação do Documento | Abdul Hannan ,Heitor marques,Josue Tixeira,Hugo rocha |
| 16/04/2024 | 1.1 | Sprint 1  | Abdul Hannan|

